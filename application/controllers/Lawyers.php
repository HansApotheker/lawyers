<?php

class Lawyers extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('lawyer_model');
    }

    public function index()
    {
        $this->load->view('index');
    }

    const SEG_DIFF = 3;

    public function index_ajax()
    {
        $types_count = count(Lawyer_model::$types);
        $types = array();
        for ($i = 0; $i < $types_count; $i++)
        {
            $seg = $this->uri->segment(self::SEG_DIFF + $i);
            if ($seg !== FALSE && in_array($seg, array_keys(Lawyer_model::$types)))
            {
                $types[] = $seg;
            }
        }

        $types = array_map('intval', $types);

        echo json_encode($this->lawyer_model->get_records($types));
    }

    public function add()
    {
        $this->edit();
    }

    public function edit($id = NULL)
    {
        if ( ! empty($id))
        {
            $lawyer = (array) $this->lawyer_model->get_record($id);
        }
        else
        {
            $lawyer = array();
        }

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');

        if ($this->form_validation->run())
        {
            $lawyer['name'] = $this->input->get_post('name');
            $lawyer['email'] = $this->input->get_post('email');
            $types = $this->input->get_post('types');

            if ( ! empty($types))
            {
                $lawyer['types'] = '{' . implode(',', $types) . '}';
            }
            else
            {
                $lawyer['types'] = '{}';
            }

            if ( ! empty($id))
            {
                $this->lawyer_model->update_record($lawyer);
            }
            else
            {
                $this->lawyer_model->add_record($lawyer);
            }

            redirect('lawyers/index');
        }
        else
        {
            $this->load->view('edit', array('lawyer' => $lawyer));
        }
    }
    public function delete($id)
    {
        echo 'delete: ' . $id;
    }

} 