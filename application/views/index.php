<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript">

        var type_names = <?php echo json_encode(Lawyer_model::$types) ?>;

        $(document).ready(function(){

            load_data('', type_names);

            $('.type').click(function(){
                var items = [];
                $('.type').each(function(i, el){
                    if (el.checked)
                    {
                        items.push(el.value);
                    }
                });
                load_data(items.join('/'), type_names);
            });
        });

        function load_data(types, type_names)
        {
            $.getJSON("/lawyers/index_ajax/" + types, function( data ) {
                var items = [];
                $.each(data, function( key, val ) {

                    var names_str = '';
                    if (val.types != null)
                    {
                        var curr_types = val.types.replace(/{/g, '');
                        curr_types = curr_types.replace(/}/g, '');
                        curr_types = curr_types.split(',');

                        var names = [];
                        for (var i in curr_types)
                        {
                            var type = curr_types[i];
                            var name = type_names[type];
                            names.push(name);
                        }

                        names_str = names.join(', ')
                    }

                    items.push( "<tr><td><a href='/lawyers/edit/" + val.id + "'>" + val.name + "</a></td><td>" + val.email + "</td><td>" + names_str + "</td></tr>" );
                });
                $("table#results tbody").html(items.join(''));
            });
        }

    </script>
    <title>Welcome to CodeIgniter</title>

    <style type="text/css">

        ::selection { background-color: #E13300; color: white; }
        ::-moz-selection { background-color: #E13300; color: white; }

        body {
            background-color: #fff;
            margin: 40px;
            font: 13px/20px normal Helvetica, Arial, sans-serif;
            color: #4F5155;
        }

        a {
            color: #003399;
            background-color: transparent;
            font-weight: normal;
        }

        h1 {
            color: #444;
            background-color: transparent;
            border-bottom: 1px solid #D0D0D0;
            font-size: 19px;
            font-weight: normal;
            margin: 0 0 14px 0;
            padding: 14px 15px 10px 15px;
        }

        code {
            font-family: Consolas, Monaco, Courier New, Courier, monospace;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            color: #002166;
            display: block;
            margin: 14px 0 14px 0;
            padding: 12px 10px 12px 10px;
        }

        #body {
            margin: 0 15px 0 15px;
        }

        p.footer {
            text-align: right;
            font-size: 11px;
            border-top: 1px solid #D0D0D0;
            line-height: 32px;
            padding: 0 10px 0 10px;
            margin: 20px 0 0 0;
        }

        #container {
            margin: 10px;
            border: 1px solid #D0D0D0;
            box-shadow: 0 0 8px #D0D0D0;
        }
    </style>
</head>
<body>

<?php foreach(Lawyer_model::$types as $id => $type): ?>
    <span><input type="checkbox" class="type" value="<?php echo $id; ?>" /><?php echo $type; ?></span>
<?php endforeach; ?>

<div id="container">
    <table id="results">
        <thead>
        <tr>
            <td>ФИО</td>
            <td>Email</td>
            <td>Направления</td>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>

</body>
</html>