<?php

class Lawyer_model extends CI_Model {

    protected $table = 'lawyer';

    const HOUSING_LAW   = 1;
    const FAMILY_LAW    = 2;
    const BANKING_LAW   = 3;
    const TRANSPORT_LAW = 4;
    const NOTARIATE     = 5;
    const ADVOCACY      = 6;

    public static $types = array(
        self::HOUSING_LAW   => 'Жилищное право',
        self::FAMILY_LAW    => 'Семейное право',
        self::BANKING_LAW   => 'Банковское право',
        self::TRANSPORT_LAW => 'Транспортное право',
        self::NOTARIATE     => 'Нотариат',
        self::ADVOCACY      => 'Адвокатура',
    );

    public function __construct()
    {
        parent::__construct();
    }

    public function get_records($types = array())
    {
        if ( ! empty($types))
        {
            $this->db->where(array('types @>' => 'array[' . join(',', $types) . ']'), NULL, FALSE);
        }

        return $this->db->get($this->table)->result();
    }

    function get_record($id)
    {
        $this->db->where(array('id' => $id));
        return $this->db->get($this->table)->row();
    }

    function add_record($data)
    {
        $this->db->insert($this->table, $data);
    }

    function update_record($data)
    {
        $this->db->where(array('id' => $data['id']));
        $this->db->update('lawyer', $data);
    }

    function delete_record($id)
    {
        $this->db->where(array('id' => $id));
        $this->db->delete('lawyer');
    }
}